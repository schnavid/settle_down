use crate::{server::ServerState, systems::executor::SystemsExecutor};

pub mod executor;

pub trait System {
    fn run(&self, state: &mut ServerState) -> crate::Result<()>;
}

pub fn create_executor() -> SystemsExecutor {
    use crate::network::systems as network;

    SystemsExecutor::new().with(&network::poll_new_connections, 4)
}
