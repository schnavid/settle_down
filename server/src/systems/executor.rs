use crate::{server::ServerState, systems::System};

pub struct SystemsExecutor {
    systems:      Vec<(Box<dyn System>, u16)>,
    once_systems: Vec<Box<dyn System>>,
    tick:         usize,
}

impl SystemsExecutor {
    pub fn new() -> SystemsExecutor {
        SystemsExecutor {
            systems:      Vec::new(),
            once_systems: Vec::new(),
            tick:         0,
        }
    }

    pub fn with<S: System + Copy + Clone + 'static>(
        mut self,
        system: &S,
        freq: u16,
    ) -> SystemsExecutor {
        self.systems.push((Box::new(*system), freq));
        self
    }

    pub fn with_once<S: System + Copy + Clone + 'static>(mut self, system: &S) -> SystemsExecutor {
        self.once_systems.push(Box::new(*system));
        self
    }

    pub fn run(&mut self, state: &mut ServerState) -> crate::Result<()> {
        if self.tick == 0 {
            for system in self.once_systems.drain(..) {
                system.run(state)?;
            }
        }

        for (system, _) in self
            .systems
            .iter()
            .filter(|(_, f)| self.tick % *f as usize == 0)
        {
            system.run(state)?;
        }

        self.tick += 1;

        Ok(())
    }
}

impl Default for SystemsExecutor {
    fn default() -> Self {
        SystemsExecutor::new()
    }
}
