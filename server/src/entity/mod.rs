use glam::{Quat, Vec3, Vec3A};

#[derive(Debug)]
pub struct Translation {
    pub position: Vec3A,
    pub rotation: Quat,
}

impl Translation {
    pub fn new() -> Translation {
        Default::default()
    }
}

impl Default for Translation {
    fn default() -> Self {
        Translation {
            position: Vec3A::splat(0.0),
            rotation: Quat::from_axis_angle(Vec3::new(0.0, 0.0, 1.0), 0.0),
        }
    }
}

#[derive(Debug)]
pub struct Physics {
    pub velocity: Vec3A,
    pub gravity:  f32,
    pub hitbox:   Option<Hitbox>,
}

impl Physics {
    pub fn new() -> Physics {
        Default::default()
    }
}

impl Default for Physics {
    fn default() -> Self {
        Physics {
            velocity: Vec3A::splat(0.0),
            gravity:  0.1,
            hitbox:   None,
        }
    }
}

#[derive(Debug)]
pub struct Hitbox {
    pub min: Vec3A,
    pub max: Vec3A,
}

#[derive(Debug)]
pub struct AnimateBeing {
    pub name:       String,
    pub health:     u32,
    pub max_health: u32,
}
