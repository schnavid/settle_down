use failure::ResultExt;
use log::Level;
pub use settle_down_core::Result;
use tokio::runtime;

pub mod chunk;
pub mod config;
pub mod entity;
pub mod network;
pub mod physics;
pub mod player;
pub mod server;
pub mod systems;

fn main() -> Result<()> {
    if cfg!(debug_assertions) {
        simple_logger::init_with_level(Level::Debug)?;
    } else {
        simple_logger::init_with_level(Level::Info)?;
    }

    let mut runtime = runtime::Builder::new()
        .threaded_scheduler()
        .enable_all()
        .build()
        .context("Failed to start Tokio runtime!")?;

    let handle = runtime.handle().clone();

    runtime.block_on(async move {
        server::main(handle).await.unwrap();
    });

    Ok(())
}
