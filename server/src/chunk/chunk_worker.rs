use crate::{config::Config, Result};
use crossbeam::channel::{Receiver, Sender};
use settle_down_core::{
    fs::region::{RegionFile, RegionPosition},
    world::chunk::{Chunk, ChunkPosition},
};
use std::{
    collections::{HashMap, HashSet},
    path::PathBuf,
    sync::Arc,
    thread,
};

#[derive(Debug)]
pub enum ChunkWorkerReply {
    LoadedChunk(ChunkPosition, Result<Option<Chunk>>),
    SavedChunk(ChunkPosition, Result<()>),
}

#[derive(Debug)]
pub enum ChunkWorkerRequest {
    LoadChunk(ChunkPosition),
    SaveChunk(Chunk),
    ShutDown,
}

#[derive(Debug)]
pub struct ChunkWorker {
    world_dir:    PathBuf,
    sender:       Sender<ChunkWorkerReply>,
    receiver:     Receiver<ChunkWorkerRequest>,
    region_files: HashMap<RegionPosition, RegionFile>,
}

impl ChunkWorker {
    fn new(
        config: Arc<Config>,
        sender: Sender<ChunkWorkerReply>,
        receiver: Receiver<ChunkWorkerRequest>,
    ) -> ChunkWorker {
        ChunkWorker {
            world_dir: config.world_dir.clone().into(),
            sender,
            receiver,
            region_files: Default::default(),
        }
    }

    pub fn start(config: Arc<Config>) -> ChunkWorkerHandle {
        let (request_tx, request_rx) = crossbeam::channel::unbounded();
        let (reply_tx, reply_rx) = crossbeam::channel::unbounded();

        let chunk_worker = ChunkWorker::new(config, reply_tx, request_rx);

        thread::Builder::new()
            .name("ChunkWorker".to_string())
            .spawn(move || ChunkWorker::run(chunk_worker))
            .expect("Unable to start ChunkWorker thread!");

        ChunkWorkerHandle {
            sender:         request_tx,
            receiver:       reply_rx,
            chunks_loading: Default::default(),
            chunks_saving:  Default::default(),
        }
    }

    fn load_chunk(&mut self, position: ChunkPosition) -> crate::Result<Option<Chunk>> {
        let region_position = RegionPosition::from_chunk(position);

        self.get_mut_region(region_position)?.load_chunk(position)
    }

    fn save_chunk(&mut self, chunk: Chunk) -> crate::Result<()> {
        let region_position = RegionPosition::from_chunk(chunk.get_position());

        self.get_mut_region(region_position)?.save_chunk(&chunk)
    }

    #[allow(clippy::map_entry)]
    fn get_mut_region(&mut self, region_position: RegionPosition) -> Result<&mut RegionFile> {
        if !self.region_files.contains_key(&region_position) {
            self.region_files.insert(
                region_position,
                RegionFile::load_or_create(&self.world_dir, region_position)?,
            );
        }

        Ok(self.region_files.get_mut(&region_position).unwrap())
    }

    fn run(mut chunk_worker: ChunkWorker) {
        while let Ok(request) = chunk_worker.receiver.recv() {
            match request {
                ChunkWorkerRequest::LoadChunk(position) => {
                    let chunk = chunk_worker.load_chunk(position);

                    chunk_worker
                        .sender
                        .send(ChunkWorkerReply::LoadedChunk(position, chunk))
                        .unwrap();
                }
                ChunkWorkerRequest::SaveChunk(chunk) => {
                    let pos = chunk.get_position();
                    let res = chunk_worker.save_chunk(chunk);

                    chunk_worker
                        .sender
                        .send(ChunkWorkerReply::SavedChunk(pos, res))
                        .unwrap();
                }
                ChunkWorkerRequest::ShutDown => break,
            }
        }
    }
}

#[derive(Debug)]
pub struct ChunkWorkerHandle {
    pub sender:     Sender<ChunkWorkerRequest>,
    pub receiver:   Receiver<ChunkWorkerReply>,
    chunks_loading: HashSet<ChunkPosition>,
    chunks_saving:  HashSet<ChunkPosition>,
}

impl ChunkWorkerHandle {
    pub fn load_chunk(&mut self, position: ChunkPosition) {
        if !self.chunks_loading.contains(&position) {
            self.chunks_loading.insert(position);
            self.sender
                .send(ChunkWorkerRequest::LoadChunk(position))
                .unwrap();
        }
    }

    pub fn save_chunk(&mut self, chunk: Chunk) {
        if !self.chunks_saving.contains(&chunk.get_position()) {
            self.chunks_saving.insert(chunk.get_position());
            self.sender
                .send(ChunkWorkerRequest::SaveChunk(chunk))
                .unwrap();
        }
    }

    pub fn receive_reply(&mut self) -> Option<ChunkWorkerReply> {
        match self.receiver.try_recv() {
            Ok(reply) => {
                match &reply {
                    ChunkWorkerReply::LoadedChunk(pos, _) => self.chunks_loading.remove(pos),
                    ChunkWorkerReply::SavedChunk(pos, _) => self.chunks_saving.remove(pos),
                };
                Some(reply)
            }
            Err(err) => {
                assert!(err.is_empty());
                None
            }
        }
    }

    pub fn shutdown(self) {
        self.sender.send(ChunkWorkerRequest::ShutDown).unwrap();
    }
}
