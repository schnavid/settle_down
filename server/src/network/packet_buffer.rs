use legion::entity::Entity;
use parking_lot::{Mutex, RwLock};
use settle_down_core::network::packet::Packet;
use smallvec::{smallvec, SmallVec};
use std::collections::HashMap;

pub struct PacketBuffers {
    buffers: Vec<PacketBuffer>,
}

type PacketBuffer = RwLock<HashMap<Entity, Mutex<SmallVec<[Packet; 2]>>>>;

impl PacketBuffers {
    pub fn new() -> PacketBuffers {
        PacketBuffers {
            buffers: Packet::used_ids()
                .into_iter()
                .map(|_| RwLock::new(HashMap::new()))
                .collect(),
        }
    }

    pub fn push_packet(&self, entity: Entity, packet: Packet) {
        let map = self.buffers[packet.id() as usize].read();

        if let Some(x) = map.get(&entity) {
            x.lock().push(packet);
        } else {
            drop(map);
            self.buffers[packet.id() as usize]
                .write()
                .insert(entity, Mutex::new(smallvec![packet]));
        }
    }

    pub fn get_packets_for_entity(
        &self,
        packet_id: u16,
        entity: Entity,
    ) -> impl Iterator<Item = Packet> {
        let map = self.buffers[packet_id as usize].read();

        if let Some(x) = map.get(&entity) {
            let x = x.lock().drain(..).collect::<SmallVec<[Packet; 2]>>();

            x.into_iter()
        } else {
            SmallVec::new().into_iter()
        }
    }
}

impl Default for PacketBuffers {
    fn default() -> Self {
        PacketBuffers::new()
    }
}
