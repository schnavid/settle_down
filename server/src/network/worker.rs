use crate::network::{
    packet_buffer::PacketBuffers, ListenerToServerMessage, NewClientInfo, ServerToListenerMessage,
};
use failure::Context;
use flume::{Receiver, Sender};
use futures::{future::Either, SinkExt};
use legion::entity::Entity;
use settle_down_core::network::{codec::SettleDownCodec, packet::Packet};
use std::{net::SocketAddr, sync::Arc};
use tokio::{net::TcpStream, stream::StreamExt};
use tokio_util::codec::Framed;

#[derive(Debug)]
pub enum ServerToWorkerMessage {
    SendPacket(Packet),
    Disconnect,
    PlayerLoggedIn(String),
}

#[derive(Debug)]
pub enum WorkerToServerMessage {}

#[allow(dead_code)]
pub struct NetworkWorker {
    framed: Framed<TcpStream, SettleDownCodec>,
    addr:   SocketAddr,
    tx:     Sender<WorkerToServerMessage>,
    rx:     Receiver<ServerToWorkerMessage>,

    server_tx: Option<Sender<ServerToWorkerMessage>>,
    server_rx: Option<Receiver<WorkerToServerMessage>>,

    listener_tx: Sender<ListenerToServerMessage>,
    listener_rx: Receiver<ServerToListenerMessage>,

    entity:        Entity,
    packet_buffer: Arc<PacketBuffers>,
}

impl NetworkWorker {
    pub async fn run(
        stream: TcpStream,
        addr: SocketAddr,
        listener_tx: Sender<ListenerToServerMessage>,
        listener_rx: Receiver<ServerToListenerMessage>,
        packet_buffer: Arc<PacketBuffers>,
    ) -> crate::Result<()> {
        let codec = SettleDownCodec::new();
        let framed = Framed::new(stream, codec);

        let (server_tx, rx) = flume::unbounded();
        let (tx, server_rx) = flume::unbounded();

        let entity = request_entity(&listener_tx, &listener_rx).await?;

        let mut worker = NetworkWorker {
            framed,
            addr,
            tx,
            rx,
            server_tx: Some(server_tx),
            server_rx: Some(server_rx),
            listener_tx,
            listener_rx,
            entity,
            packet_buffer,
        };

        worker.run_impl().await?;

        Ok(())
    }

    async fn run_impl(&mut self) -> crate::Result<()> {
        loop {
            let received_msg = self.rx.recv_async();
            let received_packet = self.framed.next();

            let select = futures::future::select(received_msg, received_packet);

            match select.await {
                Either::Left((msg, _)) => {
                    let msg = msg?;

                    match msg {
                        ServerToWorkerMessage::SendPacket(packet) => {
                            self.framed.send(packet).await?
                        }
                        ServerToWorkerMessage::Disconnect => {
                            self.listener_tx
                                .send(ListenerToServerMessage::DestroyPlayerEntity(self.entity))?;
                            self.framed.send(Packet::Disconnect).await?;
                            return Ok(());
                        }
                        ServerToWorkerMessage::PlayerLoggedIn(name) => {
                            self.framed.send(Packet::LoginSuccess(name)).await?;
                        }
                    }
                }
                Either::Right((packet, _)) => match packet {
                    Some(packet) => {
                        log::debug!("Received Packet: {:?}", packet);
                        let packet = packet?;

                        match packet {
                            Packet::Ping => {
                                log::debug!("Received Ping, sending Pong.");
                                self.framed.send(Packet::Pong).await?;
                            }
                            Packet::Login(name) => {
                                assert!(
                                    self.server_tx.is_some() && self.server_rx.is_some(),
                                    "Player already logged in"
                                );
                                self.listener_tx.send(ListenerToServerMessage::NewClient(
                                    NewClientInfo {
                                        entity: self.entity,
                                        name,
                                        addr: self.addr,
                                        tx: self.server_tx.take().unwrap(),
                                        rx: self.server_rx.take().unwrap(),
                                    },
                                ))?;
                            }
                            _ => self.packet_buffer.push_packet(self.entity, packet),
                        }
                    }
                    None => {
                        log::info!("Client {} disconnected!", self.addr.to_string());
                        self.listener_tx
                            .send(ListenerToServerMessage::DestroyPlayerEntity(self.entity))?;
                        return Err(Context::from("Client Disconnected!").into());
                    }
                },
            }

            tokio::task::yield_now().await;
        }
    }
}

async fn request_entity(
    listener_tx: &Sender<ListenerToServerMessage>,
    listener_rx: &Receiver<ServerToListenerMessage>,
) -> crate::Result<Entity> {
    listener_tx.send(ListenerToServerMessage::RequestPlayerEntity)?;

    match listener_rx.recv_async().await? {
        ServerToListenerMessage::PlayerEntity(entity) => Ok(entity),
    }
}
