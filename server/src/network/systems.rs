use crate::{
    network::{ListenerToServerMessage, NetworkManager, ServerToListenerMessage},
    player::make_player,
};
use legion::world::World;
use settle_down_server_macros::system;

#[system]
pub fn poll_new_connections(network: &mut NetworkManager, world: &mut World) -> crate::Result<()> {
    if let Ok(msg) = network.server_rx.try_recv() {
        match msg {
            ListenerToServerMessage::NewClient(info) => {
                log::info!("New Client! {}", info.addr.to_string());
                make_player(info, world)?;
            }
            ListenerToServerMessage::RequestPlayerEntity => {
                let entity = world.insert((), std::iter::once(()))[0];
                network
                    .server_tx
                    .send(ServerToListenerMessage::PlayerEntity(entity))?;
            }
            ListenerToServerMessage::DestroyPlayerEntity(entity) => {
                log::debug!("Destroying Player Entity {}!", entity);
                world.delete(entity);
            }
        }
    }

    Ok(())
}
