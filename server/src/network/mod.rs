use crate::{
    config::Config,
    network::{
        packet_buffer::PacketBuffers,
        worker::{NetworkWorker, ServerToWorkerMessage, WorkerToServerMessage},
    },
};
use flume::{Receiver, Sender};
use legion::entity::Entity;
use std::{net::SocketAddr, sync::Arc};
use tokio::net::TcpListener;

pub mod packet_buffer;
pub mod systems;
pub mod worker;

#[derive(Debug)]
pub enum ServerToListenerMessage {
    PlayerEntity(Entity),
}

#[derive(Debug)]
pub enum ListenerToServerMessage {
    NewClient(NewClientInfo),
    RequestPlayerEntity,
    DestroyPlayerEntity(Entity),
}

#[derive(Debug)]
pub struct NewClientInfo {
    pub(crate) entity: Entity,
    pub(crate) name:   String,
    pub(crate) addr:   SocketAddr,
    pub(crate) tx:     Sender<ServerToWorkerMessage>,
    pub(crate) rx:     Receiver<WorkerToServerMessage>,
}

#[derive(Debug)]
pub struct NetworkManager {
    pub(crate) server_tx: Sender<ServerToListenerMessage>,
    pub(crate) server_rx: Receiver<ListenerToServerMessage>,
}

impl NetworkManager {
    async fn run(
        mut listener: TcpListener,
        tx: Sender<ListenerToServerMessage>,
        rx: Receiver<ServerToListenerMessage>,
        packet_buffer: Arc<PacketBuffers>,
    ) -> crate::Result<()> {
        loop {
            let (stream, addr) = listener.accept().await?;

            log::info!("New Connection from {}", &addr);

            tokio::spawn(NetworkWorker::run(
                stream,
                addr,
                tx.clone(),
                rx.clone(),
                packet_buffer.clone(),
            ));

            tokio::task::yield_now().await
        }
    }

    pub async fn start(
        config: Arc<Config>,
        packet_buffer: Arc<PacketBuffers>,
    ) -> crate::Result<NetworkManager> {
        let addr = format!("{}:{}", config.server.address, config.server.port);

        let listener = TcpListener::bind(addr.clone()).await?;

        log::info!("Listening on {}.", addr);

        let (listener_tx, server_rx) = flume::bounded(16);
        let (server_tx, listener_rx) = flume::bounded(16);

        let manager = NetworkManager {
            server_rx,
            server_tx,
        };

        tokio::spawn(NetworkManager::run(
            listener,
            listener_tx,
            listener_rx,
            packet_buffer,
        ));

        Ok(manager)
    }
}
