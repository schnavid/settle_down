use crate::{server::ServerState, systems};
use spin_sleep::LoopHelper;

pub fn main_loop(state: &mut ServerState) -> crate::Result<()> {
    let mut loop_helper = LoopHelper::builder()
        .report_interval_s(2.0)
        .build_with_target_rate(60.0);

    // let mut current_tps = None;

    let mut runner = systems::create_executor();

    loop {
        loop_helper.loop_start();

        if state.shutdown_rx.try_recv().is_ok() {
            return Ok(());
        }

        runner.run(state)?;

        // if let Some(tps) = loop_helper.report_rate() {
        //     // current_tps = Some(tps.round());
        //     // log::debug!("Server running with TPS: {}!", tps);
        // }

        loop_helper.loop_sleep();
    }
}
