use crate::{
    chunk::chunk_worker::{ChunkWorker, ChunkWorkerHandle},
    config::Config,
    network::{packet_buffer::PacketBuffers, NetworkManager},
};
use failure::_core::fmt::Formatter;
use legion::world::{Universe, World};
use settle_down_core::world::chunk_map::ChunkMap;
use std::{fmt::Debug, sync::Arc, thread};
use tokio::{runtime, sync::oneshot};

pub mod main_loop;

#[allow(dead_code)]
pub struct ServerState {
    shutdown_rx:               crossbeam::Receiver<()>,
    chunk_worker_handle:       ChunkWorkerHandle,
    pub(crate) chunk_map:      ChunkMap,
    pub(crate) network:        NetworkManager,
    universe:                  Universe,
    pub(crate) world:          World,
    pub(crate) packet_buffers: Arc<PacketBuffers>,
}

impl Debug for ServerState {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "ServerState {{ .. }}")
    }
}

impl ServerState {
    fn shutdown(self) {
        self.chunk_worker_handle.shutdown();
    }
}

pub async fn main(_runtime: runtime::Handle) -> crate::Result<()> {
    log::info!("Starting Server...");

    let config = Arc::new(Config::load().await);

    let packet_buffer = Arc::new(PacketBuffers::new());

    let chunk_worker_handle = ChunkWorker::start(config.clone());

    let shutdown_rx = shutdown_init();

    let chunk_map = ChunkMap::new();

    let network = NetworkManager::start(config.clone(), packet_buffer.clone()).await?;

    let universe = Universe::new();
    let world = universe.create_world();

    let mut state = ServerState {
        shutdown_rx,
        chunk_worker_handle,
        chunk_map,
        network,
        universe,
        world,
        packet_buffers: packet_buffer,
    };

    log::info!("Running Server...");

    state = run_main_loop(state).await;

    log::info!("Shutting down Server...");

    state.shutdown();

    log::info!("Server exited successfully!");

    Ok(())
}

fn shutdown_init() -> crossbeam::Receiver<()> {
    let (shutdown_tx, shutdown_rx) = crossbeam::bounded(1);

    ctrlc::set_handler(move || shutdown_tx.try_send(()).unwrap()).unwrap();

    shutdown_rx
}

async fn run_main_loop(mut state: ServerState) -> ServerState {
    let (tx, rx) = oneshot::channel();

    thread::Builder::new()
        .name("settle_down".to_string())
        .spawn(move || {
            main_loop::main_loop(&mut state).unwrap();

            tx.send(state).unwrap();
        })
        .unwrap();

    rx.await.unwrap()
}
