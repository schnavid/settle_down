use proc_macro::TokenStream;
use quote::{quote, ToTokens};
use std::collections::HashMap;
use syn::{parse_macro_input, FnArg, ItemFn, Pat, Type};

#[proc_macro_attribute]
pub fn system(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let input: ItemFn = parse_macro_input!(item as ItemFn);

    let map: HashMap<_, _> = vec![
        (quote! { World }.to_string(), quote! { world }),
        (quote! { NetworkManager }.to_string(), quote! { network }),
        (
            quote! { Arc<PacketBuffers> }.to_string(),
            quote! { packet_buffers },
        ),
        (quote! { ChunkMap }.to_string(), quote! { chunk_map }),
    ]
    .into_iter()
    .collect();

    let signature = &input.sig;
    let params = signature
        .inputs
        .iter()
        .map(|arg| match arg {
            FnArg::Receiver(_) => unreachable!(),
            FnArg::Typed(pt) => pt,
        })
        .map(|param| {
            let reference = match &*param.ty {
                Type::Reference(r) => r,
                _ => unreachable!(),
            };

            let inner = match &*reference.elem {
                Type::Path(p) => p,
                _ => unreachable!(),
            };

            let ty = inner.path.segments.last().unwrap();

            let mutability = reference.mutability;

            let ident = match &*param.pat {
                Pat::Ident(ident) => ident.ident.clone(),
                _ => unreachable!(),
            };

            let field = match map.get(&ty.to_token_stream().to_string()) {
                None => panic!(
                    "Unexpected System Parameter: {}",
                    reference.to_token_stream()
                ),
                Some(x) => x,
            };

            quote! { let #ident = &#mutability state.#field as #reference; }
        })
        .collect::<Vec<_>>();

    let name = signature.ident.clone();
    let content = &input.block;

    let res = quote! {
        #[allow(non_camel_case_types)]
        #[derive(Clone, Copy)]
        pub struct #name;

        impl crate::systems::System for #name {
            fn run(&self, state: &mut crate::server::ServerState) -> crate::Result<()> {
                #(#params)*

                #content
            }
        }
    };

    res.into()
}
