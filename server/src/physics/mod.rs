use crate::entity::{Physics, Translation};
use legion::{
    query::{IntoQuery, Write},
    world::World,
};
use settle_down_core::world::chunk_map::ChunkMap;
use settle_down_server_macros::system;

#[system]
pub fn physics_entity_movement(world: &mut World, chunk_map: &ChunkMap) -> crate::Result<()> {
    let query = <(Write<Translation>, Write<Physics>)>::query();
    query.par_entities_for_each_mut(world, |(entity, (mut translation, mut physics))| {
        let velocity = &mut physics.velocity;

        let next_position = translation.position + *velocity;
    });

    Ok(())
}
