use crate::{
    entity::{AnimateBeing, Physics, Translation},
    network::{
        worker::{ServerToWorkerMessage, WorkerToServerMessage},
        NewClientInfo,
    },
};
use flume::{Receiver, Sender};
use legion::world::World;
use std::net::SocketAddr;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct PlayerTag;

#[derive(Debug)]
pub struct Network {
    addr: SocketAddr,
    tx:   Sender<ServerToWorkerMessage>,
    rx:   Receiver<WorkerToServerMessage>,
}

impl Network {
    pub fn new(
        addr: SocketAddr,
        tx: Sender<ServerToWorkerMessage>,
        rx: Receiver<WorkerToServerMessage>,
    ) -> Network {
        Network { addr, tx, rx }
    }
}

pub fn make_player(info: NewClientInfo, world: &mut World) -> crate::Result<()> {
    let entity = info.entity;
    world.add_tag(entity, PlayerTag)?;
    world.add_component(entity, Translation::new())?;
    world.add_component(entity, Physics::new())?;
    world.add_component(entity, Network::new(info.addr, info.tx.clone(), info.rx))?;
    world.add_component(
        entity,
        AnimateBeing {
            name:       info.name.clone(),
            health:     100,
            max_health: 100,
        },
    )?;
    info.tx
        .send(ServerToWorkerMessage::PlayerLoggedIn(info.name))?;
    Ok(())
}
