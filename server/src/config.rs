use failure::ResultExt;
use serde::Deserialize;
use tokio::{fs::File, io::AsyncReadExt};

#[derive(Debug, Deserialize)]
pub struct Config {
    pub world_dir: String,
    pub server:    ConfigServer,
}

#[derive(Debug, Deserialize)]
pub struct ConfigServer {
    pub address: String,
    pub port:    String,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            world_dir: "world0".to_string(),
            server:    ConfigServer {
                address: "127.0.0.1".to_string(),
                port:    "8080".to_string(),
            },
        }
    }
}

impl Config {
    pub async fn load() -> Config {
        async fn load_inner() -> crate::Result<Config> {
            let mut str = String::new();
            File::open("config.toml")
                .await
                .context("Failed to open config.toml!")?
                .read_to_string(&mut str)
                .await
                .context("Failed to read config.toml!")?;

            let config = toml::from_str(str.as_str())?;

            Ok(config)
        }

        match load_inner().await {
            Ok(config) => config,
            Err(e) => {
                log::info!("Encountered Error while reading config.toml: {}", e);
                log::info!("Using default config.");
                Config::default()
            }
        }
    }
}
