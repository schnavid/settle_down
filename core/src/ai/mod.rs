use crate::world::WorldCoordinates;

pub mod pathing;

pub enum Task {
    Movement(WorldCoordinates),
}
