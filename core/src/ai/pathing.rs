use crate::world::{World, WorldCoordinates};
use std::{
    cell::RefCell,
    cmp::Ordering,
    rc::Rc,
    sync::{Arc, Mutex},
};

#[derive(Debug)]
pub struct Path {
    start:            WorldCoordinates,
    end:              WorldCoordinates,
    pub(crate) steps: Vec<WorldCoordinates>,
}

#[derive(Clone, Debug)]
struct PathNode {
    coordinates: WorldCoordinates,
    parent:      Option<Rc<RefCell<PathNode>>>,
    g_cost:      f32,
    h_cost:      f32,
}

#[inline]
fn neighbors(of: WorldCoordinates) -> [WorldCoordinates; 4] {
    [
        (of.0 + 1, of.1, of.2),
        (of.0, of.1, of.2 + 1),
        (of.0 - 1, of.1, of.2),
        (of.0, of.1, of.2 - 1),
    ]
}

fn heuristic(start: WorldCoordinates, end: WorldCoordinates) -> f32 {
    // (end.0 - start.0).abs() as usize + (end.1 - start.1) as usize + (end.2 -
    // start.2).abs() as usize
    ((end.0 - start.0).pow(2) as f32
        + (end.1 - start.1).pow(2) as f32
        + (end.2 - start.2).pow(2) as f32)
        .sqrt()
}

pub fn find_path(
    world: &Arc<Mutex<World>>,
    start: WorldCoordinates,
    end: WorldCoordinates,
) -> Option<Path> {
    let mut path = Path {
        start,
        end,
        steps: Vec::with_capacity(heuristic(start, end).ceil() as usize),
    };

    let mut open_set = Vec::new();
    let mut closed_set = Vec::new();

    let start_node = Rc::new(RefCell::new(PathNode {
        coordinates: start,
        parent:      None,
        g_cost:      0.0,
        h_cost:      heuristic(start, end),
    }));

    let mut current_node;
    open_set.push(start_node);
    while !open_set.is_empty() {
        open_set.sort_by(|a, b| {
            ({
                let a = a.as_ref().borrow();
                a.g_cost + a.h_cost
            })
            .partial_cmp(
                &({
                    let b = b.as_ref().borrow();
                    b.g_cost + b.h_cost
                }),
            )
            .unwrap_or(Ordering::Equal)
        });

        current_node = open_set.remove(0);
        let (current_coords, current_g_cost) = {
            let c = current_node.as_ref().borrow();
            (c.coordinates, c.g_cost)
        };

        closed_set.push(current_coords);

        if current_coords == end {
            while current_node.as_ref().borrow().coordinates != start {
                path.steps.push(current_node.as_ref().borrow().coordinates);
                let parent = current_node.as_ref().borrow().parent.clone().unwrap();
                current_node = parent;
            }
            path.steps.push(current_node.as_ref().borrow().coordinates);
            return Some(path);
        }

        for neighbor in neighbors(current_coords).iter().copied() {
            if !world
                .lock()
                .unwrap()
                .get_tile(neighbor)
                .map(|x| x.allow_movement(world.clone()))
                .unwrap_or(true)
                || closed_set.contains(&neighbor)
            {
                continue;
            }

            let cost = current_g_cost + heuristic(current_coords, neighbor);

            let mut x = None;
            if match open_set
                .iter()
                .find(|x| x.as_ref().borrow().coordinates == neighbor)
            {
                None => true,
                Some(y) => {
                    x = Some(y);
                    cost < y.as_ref().borrow().g_cost
                }
            } {
                match x {
                    None => open_set.push(Rc::new(RefCell::new(PathNode {
                        coordinates: neighbor,
                        parent:      Some(current_node.clone()),
                        g_cost:      cost,
                        h_cost:      heuristic(neighbor, end),
                    }))),
                    Some(x) => {
                        let mut x = x.borrow_mut();
                        x.parent = Some(current_node.clone());
                        x.g_cost = cost;
                        x.h_cost = heuristic(neighbor, end);
                    }
                }
            }
        }
    }

    None
}
