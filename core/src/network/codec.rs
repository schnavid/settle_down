use crate::network::packet::Packet;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use bytes::{buf::BufMutExt, Buf, BufMut, BytesMut};
use failure::Error;
use flate2::{read::ZlibDecoder, write::ZlibEncoder, Compression};
use std::{
    io::{Cursor, ErrorKind, Read},
    mem::size_of,
};
use tokio_util::codec::{Decoder, Encoder};

#[derive(Debug)]
pub struct SettleDownCodec {
    decompressed_buffer: Vec<u8>,
}

impl SettleDownCodec {
    pub fn new() -> SettleDownCodec {
        SettleDownCodec {
            decompressed_buffer: Vec::new(),
        }
    }
}

impl Default for SettleDownCodec {
    fn default() -> Self {
        SettleDownCodec::new()
    }
}

impl Encoder<Packet> for SettleDownCodec {
    type Error = Error;

    fn encode(&mut self, packet: Packet, dst: &mut BytesMut) -> crate::Result<()> {
        const HEADER_SIZE: usize = size_of::<u32>();

        dst.reserve(HEADER_SIZE);

        dst.extend_from_slice(&[0u8; HEADER_SIZE]);

        let mut header = dst.split_to(HEADER_SIZE);

        assert!(dst.is_empty());

        {
            let mut encoder = ZlibEncoder::new(dst.writer(), Compression::best());
            encoder.write_u16::<BigEndian>(packet.id())?;
            packet.write_to(&mut encoder)?;
        }

        assert!(!dst.is_empty());

        let data_len = dst.len();

        header.advance(0);
        header.clear();
        header.put_u32(data_len as u32);

        std::mem::swap(dst, &mut header);
        dst.unsplit(header);

        Ok(())
    }
}

impl Decoder for SettleDownCodec {
    type Error = Error;
    type Item = Packet;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Packet>, Error> {
        let mut cursor = Cursor::new(src.as_ref());

        let length = match cursor.read_u32::<BigEndian>() {
            Ok(length) => length,
            Err(e) => {
                return if e.kind() == ErrorKind::UnexpectedEof {
                    Ok(None)
                } else {
                    Err(e.into())
                }
            }
        } as usize;

        if length > cursor.remaining() {
            return Ok(None);
        }

        let position = cursor.position() as usize;
        src.advance(position);
        cursor = Cursor::new(&src[..length]);

        self.decompressed_buffer.clear();

        {
            let mut decoder = ZlibDecoder::new(cursor);
            decoder.read_to_end(&mut self.decompressed_buffer)?;
        }

        cursor = Cursor::new(&self.decompressed_buffer);

        let id = cursor.read_u16::<BigEndian>()?;

        src.advance(length);

        Ok(Some(Packet::read_from(id, &mut cursor)?))
    }
}
