use crate::{
    fs,
    world::chunk::{Chunk, ChunkPosition},
};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use failure::_core::fmt::Formatter;
use std::{
    fmt::Display,
    io::{Read, Write},
};

pub const PACKET_VERSION: u32 = 1;

pub const PACKET_PING: u16 = 0x0000;
pub const PACKET_PONG: u16 = 0x0001;
pub const PACKET_LOGIN: u16 = 0x0002;
pub const PACKET_LOGIN_SUCCESS: u16 = 0x0003;
pub const PACKET_LOAD_CHUNK: u16 = 0x0004;
pub const PACKET_DISCONNECT: u16 = 0x0005;

#[derive(Debug)]
pub enum Packet {
    Ping,
    Pong,
    Login(String),
    LoginSuccess(String),
    LoadChunk(Chunk),
    Disconnect,
}

impl Packet {
    pub fn used_ids() -> Vec<u16> {
        vec![
            PACKET_PING,
            PACKET_PONG,
            PACKET_LOGIN,
            PACKET_LOGIN_SUCCESS,
            PACKET_LOAD_CHUNK,
            PACKET_DISCONNECT,
        ]
    }

    pub fn id(&self) -> u16 {
        use Packet::*;
        match self {
            Ping => PACKET_PING,
            Pong => PACKET_PONG,
            Login(_) => PACKET_LOGIN,
            LoginSuccess(_) => PACKET_LOGIN_SUCCESS,
            LoadChunk(_) => PACKET_LOAD_CHUNK,
            Disconnect => PACKET_DISCONNECT,
        }
    }

    pub fn direction(id: u16) -> PacketDirection {
        use PacketDirection::*;
        match id {
            PACKET_PING => ToServer,
            PACKET_PONG => ToClient,
            PACKET_LOGIN => ToServer,
            PACKET_LOGIN_SUCCESS => ToClient,
            PACKET_LOAD_CHUNK => ToClient,
            PACKET_DISCONNECT => ToClient,
            _ => ToClient,
        }
    }

    pub fn write_to<W: Write>(&self, dst: &mut W) -> crate::Result<()> {
        use Packet::*;
        match self {
            Ping => {}
            Pong => {}
            Login(name) => {
                assert!(name.len() < 256);
                dst.write_u8(name.len() as u8)?;
                dst.write_all(name.as_bytes())?;
            }
            LoginSuccess(name) => {
                assert!(name.len() < 256);
                dst.write_u8(name.len() as u8)?;
                dst.write_all(name.as_bytes())?;
            }
            LoadChunk(chunk) => {
                dst.write_i32::<BigEndian>(chunk.get_position().x)?;
                dst.write_i32::<BigEndian>(chunk.get_position().z)?;
                fs::chunk::write_chunk(dst, chunk)?;
            }
            Disconnect => {}
        }

        Ok(())
    }

    pub fn read_from<R: Read>(id: u16, src: &mut R) -> crate::Result<Packet> {
        use Packet::*;

        Ok(match id {
            PACKET_PING => Ping,
            PACKET_PONG => Pong,
            PACKET_LOGIN => {
                let len = src.read_u8()?;
                let mut buf = Vec::with_capacity(len as usize);
                let actual_len = src.take(len as u64).read_to_end(&mut buf)?;
                assert_eq!(len as usize, actual_len);
                Login(String::from_utf8(buf)?)
            }
            PACKET_LOGIN_SUCCESS => {
                let len = src.read_u8()?;
                let mut buf = Vec::with_capacity(len as usize);
                let actual_len = src.take(len as u64).read_to_end(&mut buf)?;
                assert_eq!(len as usize, actual_len);
                LoginSuccess(String::from_utf8(buf)?)
            }
            PACKET_LOAD_CHUNK => {
                let x = src.read_i32::<BigEndian>()?;
                let z = src.read_i32::<BigEndian>()?;
                let chunk = fs::chunk::read_chunk(src, ChunkPosition::new(x, z))?;
                LoadChunk(chunk)
            }
            PACKET_DISCONNECT => Disconnect,
            _ => return Err(PacketError::InvalidId.into()),
        })
    }
}

#[derive(Debug)]
pub enum PacketError {
    InvalidId,
}

impl Display for PacketError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            PacketError::InvalidId => write!(f, "Invalid Packet Id"),
        }
    }
}

impl std::error::Error for PacketError {}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum PacketDirection {
    ToClient,
    ToServer,
}
