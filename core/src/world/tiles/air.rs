use crate::world::tiles::{Color, TileDescription, TileData, TileId};
use std::io::Read;

#[derive(Debug, Copy, Clone)]
pub struct Air;

impl Air {
    pub const ID: TileId = TileId(0);
}

impl TileDescription for Air {
    fn new() -> Self
    where
        Self: Sized,
    {
        Air
    }

    fn id(&self) -> TileId {
        Self::ID
    }

    fn create_data(&self) -> TileData {
        TileData::None
    }

    fn name(&self, _data: &TileData) -> String {
        String::from("Nothing")
    }

    fn minable(&self, _data: &TileData) -> bool {
        false
    }

    fn color(&self, _data: &TileData) -> Color {
        [0., 0., 0., 0.]
    }

    fn allow_movement(&self, _data: &TileData) -> bool {
        true
    }

    fn allow_light(&self, _data: &TileData) -> bool {
        true
    }

    fn accessible_neighbors(&self, _data: &TileData) -> Vec<(i8, i8, i8, u8)> {
        vec![
            (1, 0, 0, 0),
            (0, 0, 1, 0),
            (-1, 0, 0, 0),
            (0, 0, -1, 0),
            (1, 1, 0, 2),
            (0, 1, 1, 2),
            (-1, 1, 0, 2),
            (0, 1, -1, 2),
            (1, -1, 0, 2),
            (0, -1, 1, 2),
            (-1, -1, 0, 2),
            (0, -1, -1, 2),
        ]
    }

    fn read(&self, _rdr: &mut dyn Read) -> std::io::Result<TileData> {
        Ok(TileData::None)
    }
}
