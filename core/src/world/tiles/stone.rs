use crate::world::tiles::{Color, TileDescription, TileData, TileId};
use std::io::Read;

#[derive(Debug, Copy, Clone)]
pub struct Stone;

impl Stone {
    pub const ID: TileId = TileId(1);
}

impl TileDescription for Stone {
    fn new() -> Self
    where
        Self: Sized,
    {
        Stone
    }

    fn id(&self) -> TileId {
        Self::ID
    }

    fn create_data(&self) -> TileData {
        TileData::None
    }

    fn name(&self, _data: &TileData) -> String {
        String::from("Stone Wall")
    }

    fn minable(&self, _data: &TileData) -> bool {
        true
    }

    fn color(&self, _data: &TileData) -> Color {
        [0.3, 0.3, 0.3, 1.0]
    }

    fn allow_movement(&self, _data: &TileData) -> bool {
        false
    }

    fn allow_light(&self, _data: &TileData) -> bool {
        false
    }

    fn accessible_neighbors(&self, _data: &TileData) -> Vec<(i8, i8, i8, u8)> {
        Vec::new()
    }

    fn read(&self, _rdr: &mut dyn Read) -> std::io::Result<TileData> {
        Ok(TileData::None)
    }
}
