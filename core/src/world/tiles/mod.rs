use crate::world::{
    tiles::{air::Air, stone::Stone},
    tile::Tile,
};
use byteorder::WriteBytesExt;
use lazy_static::lazy_static;
use std::{
    fmt::Debug,
    io::{Read, Write},
};

pub type Color = [f32; 4];

pub mod air;
pub mod stone;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct TileId(pub u32);

impl From<u32> for TileId {
    fn from(id: u32) -> Self {
        TileId(id)
    }
}

impl Into<u32> for TileId {
    fn into(self) -> u32 {
        self.0
    }
}

#[derive(Debug, Clone)]
pub enum TileData {
    None,
    Bool(bool),
}

impl TileData {
    pub fn write(&self, wtr: &mut dyn Write) -> std::io::Result<()> {
        match self {
            TileData::None => {}
            TileData::Bool(b) => wtr.write_u8(u8::from(*b))?,
        }

        Ok(())
    }
}

pub trait TileDescription: Debug {
    fn new() -> Self
    where
        Self: Sized;

    fn create(&self) -> Tile {
        Tile(self.id(), self.create_data())
    }

    fn register(reg: &mut Vec<Box<dyn TileDescription + Sync>>)
    where
        Self: Sized + Sync + 'static,
    {
        let tile = Self::new();

        reg.insert(tile.id().0 as usize, Box::new(tile));
    }

    fn id(&self) -> TileId;
    fn create_data(&self) -> TileData;
    fn name(&self, data: &TileData) -> String;
    fn minable(&self, data: &TileData) -> bool;
    fn color(&self, data: &TileData) -> Color;
    fn allow_movement(&self, data: &TileData) -> bool;
    fn allow_light(&self, data: &TileData) -> bool;
    fn accessible_neighbors(&self, data: &TileData) -> Vec<(i8, i8, i8, u8)>;
    fn read(&self, rdr: &mut dyn Read) -> std::io::Result<TileData>;
}

pub fn registry() -> Vec<Box<dyn TileDescription + Sync>> {
    let mut reg: Vec<Box<dyn TileDescription + Sync>> = Vec::new();

    Air::register(&mut reg);
    Stone::register(&mut reg);

    reg
}

lazy_static! {
    pub static ref REGISTRY: Vec<Box<dyn TileDescription + Sync>> = registry();
}
