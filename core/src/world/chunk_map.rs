use crate::world::chunk::{Chunk, ChunkPosition, CHUNK_WIDTH};
use hashbrown::HashMap;
use parking_lot::{RwLock, RwLockReadGuard, RwLockWriteGuard};
use std::sync::Arc;
use crate::world::tile::{TilePosition, Tile};

#[derive(Default, Debug)]
pub struct ChunkMap(HashMap<ChunkPosition, Arc<RwLock<Chunk>>>);

impl ChunkMap {
    pub fn new() -> ChunkMap {
        ChunkMap::default()
    }

    pub fn get_chunk(&self, pos: ChunkPosition) -> Option<RwLockReadGuard<Chunk>> {
        self.0.get(&pos).map(|chunk| chunk.read())
    }

    pub fn get_mut_chunk(&self, pos: ChunkPosition) -> Option<RwLockWriteGuard<Chunk>> {
        self.0.get(&pos).map(|chunk| chunk.write())
    }

    pub fn get_chunk_lock(&self, pos: ChunkPosition) -> Option<Arc<RwLock<Chunk>>> {
        self.0.get(&pos).map(Arc::clone)
    }

    pub fn get_tile(&self, pos: TilePosition) -> Option<Tile> {
        self.get_chunk(pos.chunk()).map(|chunk| chunk.get_tile((pos.x % CHUNK_WIDTH as i32) as usize, pos.y as usize, (pos.z % CHUNK_WIDTH as i32) as usize))
    }
}
