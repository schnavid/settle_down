use crate::world::{tiles::air::Air, tile::Tile};

#[derive(Default, Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ChunkPosition {
    pub x: i32,
    pub z: i32,
}

impl ChunkPosition {
    pub fn new(x: i32, z: i32) -> ChunkPosition {
        ChunkPosition { x, z }
    }
}

pub const CHUNK_WIDTH: usize = 16;
pub const CHUNK_HEIGHT: usize = 128;
pub const SECTION_HEIGHT: usize = 16;
pub const NUM_SECTIONS: usize = 8;
pub const SECTION_VOLUME: usize = SECTION_HEIGHT * CHUNK_WIDTH * CHUNK_WIDTH;
pub const CHUNK_VOLUME: usize = SECTION_VOLUME * NUM_SECTIONS;

#[derive(Debug, Default)]
pub struct Chunk {
    position: ChunkPosition,
    sections: [Option<ChunkSection>; NUM_SECTIONS],
}

impl Chunk {
    pub fn new(position: ChunkPosition) -> Chunk {
        Chunk {
            position,
            ..Default::default()
        }
    }

    pub fn with_sections(
        position: ChunkPosition,
        sections: [Option<ChunkSection>; NUM_SECTIONS],
    ) -> Chunk {
        Chunk { position, sections }
    }

    pub fn get_section(&self, index: usize) -> Option<&ChunkSection> {
        assert!(
            index < NUM_SECTIONS,
            "Invalid ChunkSection index: {}!",
            index
        );
        self.sections[index].as_ref()
    }

    pub fn get_mut_section(&mut self, index: usize) -> Option<&mut ChunkSection> {
        assert!(
            index < NUM_SECTIONS,
            "Invalid ChunkSection index: {}!",
            index
        );
        self.sections[index].as_mut()
    }

    pub fn set_section(&mut self, index: usize, section: Option<ChunkSection>) {
        assert!(
            index < NUM_SECTIONS,
            "Invalid ChunkSection index: {}!",
            index
        );
        self.sections[index] = section;
    }

    pub fn get_sections(&self) -> impl Iterator<Item = Option<&ChunkSection>> {
        self.sections.iter().map(|x| x.as_ref())
    }

    pub fn get_position(&self) -> ChunkPosition {
        self.position
    }

    pub fn get_tile(&self, x: usize, y: usize, z: usize) -> Tile {
        self.get_section(y / NUM_SECTIONS)
            .map(|section| section.get_tile(x, y % SECTION_HEIGHT, z).clone())
            .unwrap_or_else(|| Tile::new(Air::ID))
    }
}

#[derive(Debug)]
pub struct ChunkSection {
    data: Box<[Tile]>,
}

impl ChunkSection {
    pub fn new(data: Box<[Tile]>) -> ChunkSection {
        ChunkSection { data }
    }

    pub fn get_tile(&self, x: usize, y: usize, z: usize) -> &Tile {
        &self.data[tile_index(x, y, z)]
    }

    pub fn get_data(&self) -> &[Tile] {
        &self.data
    }
}

pub fn tile_index(x: usize, y: usize, z: usize) -> usize {
    assert!(
        x < 16 && y < 16 && z < 16,
        "Invalid Tile index in Section: ({} {} {})!",
        x,
        y,
        z
    );
    y * 16 * 16 + x * 16 + z
}
