use crate::world::tiles::{Color, TileData, TileId, REGISTRY};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use std::{
    fmt::Debug,
    io::{Read, Write},
};
use crate::world::chunk::{ChunkPosition, CHUNK_WIDTH};

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq)]
pub struct TilePosition {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl TilePosition {
    pub fn chunk(&self) -> ChunkPosition {
        ChunkPosition::new(self.x / CHUNK_WIDTH as i32, self.z / CHUNK_WIDTH as i32)
    }
}

#[derive(Debug, Clone)]
pub struct Tile(pub TileId, pub TileData);

#[derive(Debug)]
pub enum Direction {
    North,
    East,
    South,
    West,
    Up,
    Down,
}

impl Tile {
    pub fn new(id: TileId) -> Self {
        REGISTRY[id.0 as usize].create()
    }

    pub fn read<R: Read>(rdr: &mut R) -> crate::Result<Self> {
        let id = rdr.read_u32::<BigEndian>()?;
        let data = REGISTRY[id as usize].read(rdr)?;

        Ok(Tile(id.into(), data))
    }

    pub fn write(&self, wtr: &mut dyn Write) -> crate::Result<()> {
        wtr.write_u32::<BigEndian>(self.id().into())?;
        self.data().write(wtr)?;

        Ok(())
    }

    #[inline]
    pub fn id(&self) -> TileId {
        self.0
    }

    #[inline]
    pub fn data(&self) -> &TileData {
        &self.1
    }

    pub fn name(&self) -> String {
        REGISTRY[self.id().0 as usize].name(self.data())
    }

    pub fn minable(&self) -> bool {
        REGISTRY[self.id().0 as usize].minable(self.data())
    }

    pub fn color(&self) -> Color {
        REGISTRY[self.id().0 as usize].color(self.data())
    }

    pub fn allow_movement(&self) -> bool {
        REGISTRY[self.id().0 as usize].allow_movement(self.data())
    }

    pub fn allow_light(&self) -> bool {
        REGISTRY[self.id().0 as usize].allow_light(self.data())
    }

    pub fn accessible_neighbors(&self) -> Vec<(i8, i8, i8, u8)> {
        REGISTRY[self.id().0 as usize].accessible_neighbors(self.data())
    }
}
