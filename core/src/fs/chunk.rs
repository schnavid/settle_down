use crate::world::{
    chunk::{Chunk, ChunkPosition, ChunkSection, NUM_SECTIONS, SECTION_VOLUME},
    tile::Tile,
};
use byteorder::{ReadBytesExt, WriteBytesExt};
use std::{
    io::{Read, Write},
    ops::Add,
};

pub fn read_chunk<R: Read>(from: &mut R, position: ChunkPosition) -> crate::Result<Chunk> {
    let sectors = from.read_u8()?;
    let mut chunk = Chunk::new(position);

    (0..NUM_SECTIONS)
        .map(|n| {
            if sectors & (1 << n) > 0 {
                Some(read_chunk_section(from))
            } else {
                None
            }
        })
        .map(|x| match x {
            Some(Ok(x)) => Ok(Some(x)),
            Some(Err(e)) => Err(e),
            None => Ok(None),
        })
        .collect::<crate::Result<Vec<Option<ChunkSection>>>>()?
        .into_iter()
        .enumerate()
        .for_each(|(n, s)| chunk.set_section(n, s));

    Ok(chunk)
}

pub fn read_chunk_section<R: Read>(from: &mut R) -> crate::Result<ChunkSection> {
    Ok(ChunkSection::new(
        (0..SECTION_VOLUME)
            .map(|_| Tile::read(from))
            .collect::<crate::Result<Box<[_]>>>()?,
    ))
}

pub fn write_chunk<W: Write>(to: &mut W, chunk: &Chunk) -> crate::Result<()> {
    let sectors = chunk
        .get_sections()
        .map(|x| match x {
            Some(_) => 1,
            None => 0,
        })
        .enumerate()
        .map(|(n, x)| (x << n) as u8)
        .fold(0, u8::add);
    to.write_u8(sectors)?;

    chunk
        .get_sections()
        .filter_map(|x| x)
        .map(|x| write_chunk_section(to, x))
        .collect::<crate::Result<()>>()?;

    Ok(())
}

pub fn write_chunk_section<W: Write>(
    to: &mut W,
    chunk_section: &ChunkSection,
) -> crate::Result<()> {
    chunk_section
        .get_data()
        .iter()
        .map(|x| x.write(to))
        .collect::<crate::Result<()>>()?;

    Ok(())
}
