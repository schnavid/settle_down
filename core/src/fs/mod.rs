//! # World Format
//!
//! ## Region Files
//!
//! - Name: `r{}_{}.rgn`
//! - Content:
//!   - Header
//!     - Version Number (`u32`)
//!     - Chunk Positions in sectors of 4KB (32 * 32 * `u32`)
//!   - for each Chunk:
//!     - Header
//!     - Tiles

pub mod chunk;
pub mod region;
