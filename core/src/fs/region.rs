use super::chunk::write_chunk;
use crate::{
    fs::chunk::read_chunk,
    world::chunk::{Chunk, ChunkPosition},
};
use bitvec::prelude::{bitvec, BitVec};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use failure::_core::fmt::{Display, Formatter};
use flate2::{read, write, Compression};
use std::{
    fs::{File, OpenOptions},
    io::{Read, Seek, SeekFrom, Write},
    path::PathBuf,
};

#[derive(Debug, Default, Copy, Clone, Ord, PartialOrd, PartialEq, Eq, Hash)]
pub struct RegionPosition {
    pub x: i32,
    pub z: i32,
}

impl RegionPosition {
    #[inline]
    pub fn new(x: i32, z: i32) -> RegionPosition {
        RegionPosition { x, z }
    }

    /// Gets the `RegionPosition` that the `ChunkPosition` resides in
    #[inline]
    pub fn from_chunk(chunk_position: ChunkPosition) -> RegionPosition {
        // Region = 32 * 32 Chunks
        RegionPosition::new(chunk_position.x / 32, chunk_position.z / 32)
    }
}

const REGION_VERSION: u32 = 1;
const REGION_CHUNK_WIDTH: usize = 32;
const REGION_CHUNK_NUM: usize = REGION_CHUNK_WIDTH * REGION_CHUNK_WIDTH;
const SECTOR_SIZE: u64 = 4096;

#[derive(Debug)]
pub enum RegionFileError {
    /// The File's version is not up-to-date
    InvalidRegionVersion(u32),
    FileDoesNotExit,
}

impl Display for RegionFileError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RegionFileError::InvalidRegionVersion(version) => write!(
                f,
                "The Region File's version is {}, but the current version is {}!",
                version, REGION_VERSION
            ),
            RegionFileError::FileDoesNotExit => write!(f, "The Region File does not exist"),
        }
    }
}

impl failure::Fail for RegionFileError {}

#[derive(Debug)]
pub struct RegionFile {
    position:     RegionPosition,
    file:         File,
    header:       RegionHeader,
    used_sectors: BitVec,
}

impl RegionFile {
    pub fn load_or_create(dir: &PathBuf, position: RegionPosition) -> crate::Result<RegionFile> {
        match RegionFile::load(dir, position) {
            Ok(r) => Ok(r),
            Err(_) => RegionFile::create(dir, position),
        }
    }

    pub fn load(dir: &PathBuf, position: RegionPosition) -> crate::Result<RegionFile> {
        let mut file = OpenOptions::new()
            .read(true)
            .write(true)
            .create(false)
            .open(region_file_path(dir, position))
            .map_err(|_| RegionFileError::FileDoesNotExit)?;

        let header = RegionHeader::read(&mut file)?;

        let mut used_sectors = bitvec![0; (file.metadata()?.len() / SECTOR_SIZE) as usize];

        for ChunkLocation { position, length } in header.chunk_locations.iter().copied() {
            (position..position + length as u32).for_each(|i| used_sectors.set(i as usize, true));
        }

        used_sectors.set(0, true);
        used_sectors.set(1, true);

        Ok(RegionFile {
            position,
            header,
            file,
            used_sectors,
        })
    }

    pub fn create(dir: &PathBuf, position: RegionPosition) -> crate::Result<RegionFile> {
        let path = region_file_path(dir, position);

        std::fs::create_dir_all(&path.parent().unwrap())?;

        let mut file = File::create(path)?;

        let header = RegionHeader::new();

        header.write(&mut file)?;

        let used_sectors = bitvec![1; 2];

        Ok(RegionFile {
            position,
            file,
            header,
            used_sectors,
        })
    }

    pub fn load_chunk(&mut self, position: ChunkPosition) -> crate::Result<Option<Chunk>> {
        let (local_x, local_z) = ((position.x % 32) as usize, (position.z % 32) as usize);

        let seek_pos = self.header.chunk_locations[local_x * 32 + local_z].position;

        if seek_pos == 0 {
            Ok(None)
        } else {
            self.file
                .seek(SeekFrom::Start(seek_pos as u64 * SECTOR_SIZE))?;

            let _size = self.file.read_u32::<BigEndian>()?;

            let mut decoder = read::ZlibDecoder::new(&mut self.file);
            let chunk = read_chunk(&mut decoder, position)?;

            Ok(Some(chunk))
        }
    }

    pub fn free_chunk_sectors(&mut self, location: ChunkLocation) {
        (location.position..location.position + location.length as u32)
            .for_each(|i| self.used_sectors.set(i as usize, false));
    }

    pub fn allocate_chunk_sectors(&mut self, min_size: usize) -> ChunkLocation {
        let (mut start, mut size) = (0, 0);

        for (position, sector) in self.used_sectors.iter().copied().enumerate() {
            if sector {
                start = 0;
                size = 0;
            } else {
                if start == 0 {
                    start = position;
                }

                size += 1;

                if size >= min_size {
                    let chunk_location = ChunkLocation {
                        position: start as u32,
                        length:   size as u8,
                    };

                    (chunk_location.position
                        ..chunk_location.position + chunk_location.length as u32)
                        .for_each(|i| self.used_sectors.set(i as usize, true));

                    return chunk_location;
                }
            }
        }

        self.used_sectors.extend(bitvec![1; min_size]);

        ChunkLocation {
            position: self.used_sectors.len() as u32,
            length:   min_size as u8,
        }
    }

    pub fn save_chunk(&mut self, chunk: &Chunk) -> crate::Result<()> {
        let position = chunk.get_position();

        let (local_x, local_z) = ((position.x % 32) as usize, (position.z % 32) as usize);

        let chunk_location = self.header.chunk_locations[local_x * 32 + local_z];

        if chunk_location.position != 0 {
            self.free_chunk_sectors(chunk_location);
        }

        let mut buf = Vec::with_capacity(SECTOR_SIZE as usize);

        let mut encoder = write::ZlibEncoder::new(&mut buf, Compression::fast());

        write_chunk(&mut encoder, chunk)?;

        let buf = encoder.finish()?;

        let len = buf.len() + 4;

        let sectors = (len + SECTOR_SIZE as usize - 1) / SECTOR_SIZE as usize;

        let chunk_location = self.allocate_chunk_sectors(sectors);

        self.file.seek(SeekFrom::Start(
            chunk_location.position as u64 * SECTOR_SIZE,
        ))?;

        self.file.write_u32::<BigEndian>(buf.len() as u32)?;

        self.file.write_all(buf)?;

        let padding = SECTOR_SIZE - (len as u64 % SECTOR_SIZE);

        for _ in 0..padding {
            self.file.write_u8(0)?;
        }

        self.header.chunk_locations[local_x * 32 + local_z] = chunk_location;

        self.save_header()
    }

    pub fn save_header(&mut self) -> crate::Result<()> {
        self.file.seek(SeekFrom::Start(0))?;

        self.header.write(&mut self.file)
    }
}

fn region_file_path(dir: &PathBuf, position: RegionPosition) -> PathBuf {
    let mut buf = dir.clone();
    buf.push(format!("region/r{}_{}.rgn", position.x, position.z));
    buf
}

#[derive(Debug)]
pub struct RegionHeader {
    version:         u32,
    chunk_locations: Vec<ChunkLocation>,
}

#[derive(Debug, Default, Copy, Clone)]
pub struct ChunkLocation {
    position: u32,
    length:   u8,
}

impl Default for RegionHeader {
    fn default() -> Self {
        RegionHeader::new()
    }
}

impl RegionHeader {
    pub fn new() -> RegionHeader {
        RegionHeader {
            version:         REGION_VERSION,
            chunk_locations: vec![Default::default(); REGION_CHUNK_NUM],
        }
    }

    pub fn read<R: Read>(from: &mut R) -> crate::Result<RegionHeader> {
        let version = from.read_u32::<BigEndian>()?;

        if version != REGION_VERSION {
            return Err(RegionFileError::InvalidRegionVersion(version).into());
        }

        let mut chunk_locations = Vec::with_capacity(REGION_CHUNK_NUM);
        for _ in 0..REGION_CHUNK_NUM {
            let position = from.read_u24::<BigEndian>()?;
            let length = from.read_u8()?;
            chunk_locations.push(ChunkLocation { position, length });
        }

        Ok(RegionHeader {
            version,
            chunk_locations,
        })
    }

    pub fn write<W: Write>(&self, to: &mut W) -> crate::Result<()> {
        to.write_u32::<BigEndian>(self.version)?;

        for ChunkLocation { position, length } in self.chunk_locations.iter().copied() {
            to.write_u24::<BigEndian>(position)?;
            to.write_u8(length)?;
        }

        Ok(())
    }
}
