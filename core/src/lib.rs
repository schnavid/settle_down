use failure::Error;

pub mod fs;
pub mod network;
pub mod world;

pub type Result<T> = core::result::Result<T, Error>;
