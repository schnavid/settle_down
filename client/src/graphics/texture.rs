use image::{load_from_memory, GenericImageView};
use wgpu::{
    AddressMode, BufferCopyView, BufferUsage, CommandEncoderDescriptor, CompareFunction, Device,
    Extent3d, FilterMode, Origin3d, Queue, Sampler, SamplerDescriptor, SwapChainDescriptor,
    TextureCopyView, TextureDescriptor, TextureDimension, TextureFormat, TextureUsage, TextureView,
};

#[derive(Debug)]
pub struct Texture {
    texture:            wgpu::Texture,
    pub(crate) view:    TextureView,
    pub(crate) sampler: Sampler,
}

impl Texture {
    pub const DEPTH_FORMAT: TextureFormat = TextureFormat::Depth32Float;

    pub fn create_diffuse_texture(device: &Device, queue: &Queue, label: &str) -> Self {
        let diffuse_bytes = include_bytes!("textures.png");
        let diffuse_image = load_from_memory(diffuse_bytes).unwrap();
        let diffuse_rgba = diffuse_image.as_rgba8().unwrap();

        let dimensions = diffuse_image.dimensions();

        let size = Extent3d {
            width:  dimensions.0,
            height: dimensions.1,
            depth:  1,
        };

        let desc = TextureDescriptor {
            label: Some(label),
            size,
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            usage: TextureUsage::SAMPLED | TextureUsage::COPY_DST,
        };
        let texture = device.create_texture(&desc);

        let buffer = device.create_buffer_with_data(&diffuse_rgba, BufferUsage::COPY_SRC);

        let mut encoder = device.create_command_encoder(&CommandEncoderDescriptor {
            label: Some("texture_buffer_copy_encoder"),
        });

        encoder.copy_buffer_to_texture(
            BufferCopyView {
                buffer:         &buffer,
                offset:         0,
                bytes_per_row:  4 * dimensions.0,
                rows_per_image: dimensions.1,
            },
            TextureCopyView {
                texture:     &texture,
                mip_level:   0,
                array_layer: 0,
                origin:      Origin3d::ZERO,
            },
            size,
        );

        queue.submit(&[encoder.finish()]);

        let view = texture.create_default_view();
        let sampler = device.create_sampler(&SamplerDescriptor {
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            address_mode_w: AddressMode::ClampToEdge,
            mag_filter:     FilterMode::Nearest,
            min_filter:     FilterMode::Nearest,
            mipmap_filter:  FilterMode::Nearest,
            lod_min_clamp:  -100.0,
            lod_max_clamp:  100.0,
            compare:        CompareFunction::Always,
        });

        Self {
            texture,
            view,
            sampler,
        }
    }

    pub fn create_depth_texture(
        device: &Device,
        sc_desc: &SwapChainDescriptor,
        label: &str,
    ) -> Self {
        let size = Extent3d {
            width:  sc_desc.width,
            height: sc_desc.height,
            depth:  1,
        };

        let desc = TextureDescriptor {
            label: Some(label),
            size,
            array_layer_count: 1,
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: Self::DEPTH_FORMAT,
            usage: TextureUsage::OUTPUT_ATTACHMENT | TextureUsage::SAMPLED | TextureUsage::COPY_SRC,
        };
        let texture = device.create_texture(&desc);

        let view = texture.create_default_view();
        let sampler = device.create_sampler(&SamplerDescriptor {
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            address_mode_w: AddressMode::ClampToEdge,
            mag_filter:     FilterMode::Linear,
            min_filter:     FilterMode::Nearest,
            mipmap_filter:  FilterMode::Nearest,
            lod_min_clamp:  -100.0,
            lod_max_clamp:  100.0,
            compare:        CompareFunction::LessEqual,
        });

        Self {
            texture,
            view,
            sampler,
        }
    }
}
