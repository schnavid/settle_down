use crate::{
    graphics::{camera::Camera, texture::Texture},
    GameState,
};
use wgpu::{
    Adapter, BindGroup, BindGroupDescriptor, BindGroupLayoutDescriptor, BindGroupLayoutEntry,
    Binding, BindingType, BlendDescriptor, BlendFactor, BlendOperation, Buffer, BufferAddress,
    BufferUsage, ColorStateDescriptor, ColorWrite, CommandEncoderDescriptor, CompareFunction,
    DepthStencilStateDescriptor, Device, LoadOp, Queue, RenderPassDepthStencilAttachmentDescriptor,
    RenderPipeline, ShaderStage, StencilStateFaceDescriptor, StoreOp, Surface, SwapChain,
    SwapChainDescriptor, TextureComponentType, TextureViewDimension,
};
use winit::dpi::PhysicalSize;

pub use vertex::Vertex;

pub mod vertex;

use crate::world::chunk::CHUNK_SIZE;
use cgmath::{Matrix4, SquareMatrix};
use winit::window::Window;

pub mod camera;
pub mod chunk_renderer;
pub mod texture;

#[derive(Debug)]
pub struct GraphicsState {
    surface:    Surface,
    adapter:    Adapter,
    device:     Device,
    queue:      Queue,
    sc_desc:    SwapChainDescriptor,
    swap_chain: SwapChain,

    render_pipeline:    RenderPipeline,
    pub(crate) camera:  Camera,
    uniforms:           Uniforms,
    uniform_buffer:     Buffer,
    uniform_bind_group: BindGroup,

    depth_texture: Texture,

    diffuse_texture:    Texture,
    diffuse_bind_group: wgpu::BindGroup,

    size: PhysicalSize<u32>,
}

impl GraphicsState {
    pub async fn new(window: &Window) -> Self {
        let size = window.inner_size();

        let surface = wgpu::Surface::create(window);

        let adapter = wgpu::Adapter::request(
            &wgpu::RequestAdapterOptions {
                power_preference:   wgpu::PowerPreference::HighPerformance,
                compatible_surface: Some(&surface),
            },
            wgpu::BackendBit::PRIMARY,
        )
        .await
        .unwrap();

        let (device, queue) = adapter
            .request_device(&wgpu::DeviceDescriptor {
                extensions: wgpu::Extensions {
                    anisotropic_filtering: false,
                },
                limits:     Default::default(),
            })
            .await;

        let sc_desc = wgpu::SwapChainDescriptor {
            usage:        wgpu::TextureUsage::OUTPUT_ATTACHMENT,
            format:       wgpu::TextureFormat::Bgra8UnormSrgb,
            width:        size.width,
            height:       size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &sc_desc);

        let vs_src = include_str!("shader.vert");
        let fs_src = include_str!("shader.frag");

        let mut compiler = shaderc::Compiler::new().unwrap();
        let vs_spirv = compiler
            .compile_into_spirv(
                vs_src,
                shaderc::ShaderKind::Vertex,
                "shader.vert",
                "main",
                None,
            )
            .unwrap();
        let fs_spirv = compiler
            .compile_into_spirv(
                fs_src,
                shaderc::ShaderKind::Fragment,
                "shader.frag",
                "main",
                None,
            )
            .unwrap();

        let vs_data = wgpu::read_spirv(std::io::Cursor::new(vs_spirv.as_binary_u8())).unwrap();
        let fs_data = wgpu::read_spirv(std::io::Cursor::new(fs_spirv.as_binary_u8())).unwrap();

        let vs_module = device.create_shader_module(&vs_data);
        let fs_module = device.create_shader_module(&fs_data);

        let camera = Camera::new(&sc_desc);

        let mut uniforms = Uniforms::new();
        uniforms.update_view_proj(&camera);
        uniforms.update_current_layer(camera.eye.y as u16);

        let uniform_buffer = device.create_buffer_with_data(
            bytemuck::cast_slice(&[uniforms]),
            wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        );

        let uniform_bind_group_layout =
            device.create_bind_group_layout(&BindGroupLayoutDescriptor {
                bindings: &[BindGroupLayoutEntry {
                    binding:    0,
                    visibility: ShaderStage::VERTEX | ShaderStage::FRAGMENT,
                    ty:         BindingType::UniformBuffer { dynamic: false },
                }],
                label:    Some("uniform_bind_group_layout"),
            });

        let uniform_bind_group = device.create_bind_group(&BindGroupDescriptor {
            layout:   &uniform_bind_group_layout,
            bindings: &[Binding {
                binding:  0,
                resource: wgpu::BindingResource::Buffer {
                    buffer: &uniform_buffer,
                    // FYI: you can share a single buffer between bindings.
                    range:  0..std::mem::size_of_val(&uniforms) as BufferAddress,
                },
            }],
            label:    Some("uniform_bind_group"),
        });

        let diffuse_texture = Texture::create_diffuse_texture(&device, &queue, "tiles_texture");

        let diffuse_texture_bind_group_layout =
            device.create_bind_group_layout(&BindGroupLayoutDescriptor {
                bindings: &[
                    BindGroupLayoutEntry {
                        binding:    0,
                        visibility: ShaderStage::FRAGMENT,
                        ty:         BindingType::SampledTexture {
                            multisampled:   false,
                            dimension:      TextureViewDimension::D2,
                            component_type: TextureComponentType::Uint,
                        },
                    },
                    BindGroupLayoutEntry {
                        binding:    1,
                        visibility: ShaderStage::FRAGMENT,
                        ty:         BindingType::Sampler { comparison: false },
                    },
                ],
                label:    Some("texture_bind_group_layout"),
            });

        let diffuse_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout:   &diffuse_texture_bind_group_layout,
            bindings: &[
                wgpu::Binding {
                    binding:  0,
                    resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
                },
                wgpu::Binding {
                    binding:  1,
                    resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
                },
            ],
            label:    Some("diffuse_bind_group"),
        });

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                bind_group_layouts: &[
                    &uniform_bind_group_layout,
                    &diffuse_texture_bind_group_layout,
                ],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            layout:                    &render_pipeline_layout,
            vertex_stage:              wgpu::ProgrammableStageDescriptor {
                module:      &vs_module,
                entry_point: "main",
            },
            fragment_stage:            Some(wgpu::ProgrammableStageDescriptor {
                module:      &fs_module,
                entry_point: "main",
            }),
            rasterization_state:       Some(wgpu::RasterizationStateDescriptor {
                front_face:             wgpu::FrontFace::Ccw,
                cull_mode:              wgpu::CullMode::None,
                depth_bias:             0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp:       0.0,
            }),
            color_states:              &[ColorStateDescriptor {
                format:      sc_desc.format,
                color_blend: BlendDescriptor {
                    src_factor: BlendFactor::SrcAlpha,
                    dst_factor: BlendFactor::OneMinusSrcAlpha,
                    operation:  BlendOperation::Add,
                },
                alpha_blend: BlendDescriptor {
                    src_factor: BlendFactor::One,
                    dst_factor: BlendFactor::One,
                    operation:  BlendOperation::Max,
                },
                write_mask:  ColorWrite::ALL,
            }],
            primitive_topology:        wgpu::PrimitiveTopology::TriangleList,
            depth_stencil_state:       Some(DepthStencilStateDescriptor {
                format:              Texture::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare:       CompareFunction::Less,
                stencil_front:       StencilStateFaceDescriptor::IGNORE,
                stencil_back:        StencilStateFaceDescriptor::IGNORE,
                stencil_read_mask:   0,
                stencil_write_mask:  0,
            }),
            vertex_state:              wgpu::VertexStateDescriptor {
                index_format:   wgpu::IndexFormat::Uint16,
                vertex_buffers: &[Vertex::desc()],
            },
            sample_count:              1,
            sample_mask:               !0,
            alpha_to_coverage_enabled: false,
        });

        let depth_texture = Texture::create_depth_texture(&device, &sc_desc, "depth_texture");

        GraphicsState {
            camera,
            surface,
            adapter,
            device,
            queue,
            render_pipeline,
            sc_desc,
            swap_chain,
            size,
            uniforms,
            uniform_buffer,
            uniform_bind_group,
            depth_texture,
            diffuse_texture,
            diffuse_bind_group,
        }
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.size = new_size;
        self.sc_desc.width = new_size.width;
        self.sc_desc.height = new_size.height;
        self.swap_chain = self.device.create_swap_chain(&self.surface, &self.sc_desc);
        self.depth_texture =
            Texture::create_depth_texture(&self.device, &self.sc_desc, "depth_texture");
    }

    pub fn update(&mut self) {
        self.uniforms.update_view_proj(&self.camera);
        self.uniforms.update_current_layer(self.camera.eye.y as u16);

        let mut encoder = self
            .device
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("update encoder"),
            });

        let staging_buffer = self.device.create_buffer_with_data(
            bytemuck::cast_slice(&[self.uniforms]),
            BufferUsage::COPY_SRC,
        );

        encoder.copy_buffer_to_buffer(
            &staging_buffer,
            0,
            &self.uniform_buffer,
            0,
            std::mem::size_of::<Uniforms>() as BufferAddress,
        );

        self.queue.submit(&[encoder.finish()]);
    }

    pub fn render(&mut self, state: &mut GameState) {
        let frame = self
            .swap_chain
            .get_next_texture()
            .expect("Timeout getting texture");

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments:        &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment:     &frame.view,
                    resolve_target: None,
                    load_op:        wgpu::LoadOp::Clear,
                    store_op:       wgpu::StoreOp::Store,
                    clear_color:    wgpu::Color {
                        r: 0.0,
                        g: 0.0,
                        b: 0.0,
                        a: 1.0,
                    },
                }],
                depth_stencil_attachment: Some(RenderPassDepthStencilAttachmentDescriptor {
                    attachment:       &self.depth_texture.view,
                    depth_load_op:    LoadOp::Clear,
                    depth_store_op:   StoreOp::Store,
                    clear_depth:      1.0,
                    stencil_load_op:  LoadOp::Clear,
                    stencil_store_op: StoreOp::Store,
                    clear_stencil:    0,
                }),
            });

            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &self.uniform_bind_group, &[]);
            render_pass.set_bind_group(1, &self.diffuse_bind_group, &[]);
            let (ex, ey, ez) = (
                (self.camera.eye.x / CHUNK_SIZE as f32) as i8,
                (self.camera.eye.y / CHUNK_SIZE as f32) as i8,
                (self.camera.eye.z / CHUNK_SIZE as f32) as i8,
            );
            for (_, renderer) in state.world.chunk_renderers.iter().filter(|(c, _)| {
                let (x, y, z) = (c.x() as i8, c.y() as i8, c.z() as i8);

                (ex - x).abs() < 2 && (ey - y) < 2 && (ez - z).abs() < 2
            }) {
                if let Some(vertex_buffer) = &renderer.vertex_buffer {
                    // println!("rendering chunk {:?} #vertices: {}", _coords,
                    // renderer.num_vertices);
                    render_pass.set_vertex_buffer(0, vertex_buffer, 0, 0);
                    render_pass.draw(0..renderer.num_vertices, 0..1);
                }
            }
        }

        self.queue.submit(&[encoder.finish()]);
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Uniforms {
    view_proj:     Matrix4<f32>,
    current_layer: u16,
}

unsafe impl bytemuck::Zeroable for Uniforms {}

unsafe impl bytemuck::Pod for Uniforms {}

impl Default for Uniforms {
    fn default() -> Self {
        Uniforms::new()
    }
}

impl Uniforms {
    pub fn new() -> Uniforms {
        Uniforms {
            view_proj:     Matrix4::identity(),
            current_layer: 127,
        }
    }

    pub fn update_view_proj(&mut self, camera: &Camera) {
        self.view_proj = camera.build_view_projection_matrix();
    }

    pub fn update_current_layer(&mut self, layer: u16) {
        self.current_layer = layer;
    }
}
