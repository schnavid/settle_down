use wgpu::{BufferAddress, InputStepMode, VertexBufferDescriptor, VertexFormat};

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Vertex {
    pub(crate) position:  [f32; 3],
    pub(crate) uv:        [f32; 2],
    pub(crate) hidden_uv: [f32; 2],
    pub(crate) shadow_uv: [f32; 2],
}

unsafe impl bytemuck::Pod for Vertex {}

unsafe impl bytemuck::Zeroable for Vertex {}

impl Vertex {
    pub fn desc<'a>() -> VertexBufferDescriptor<'a> {
        use std::mem;
        VertexBufferDescriptor {
            stride:     mem::size_of::<Vertex>() as BufferAddress,
            step_mode:  InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttributeDescriptor {
                    offset:          0,
                    shader_location: 0,
                    format:          VertexFormat::Float3,
                },
                wgpu::VertexAttributeDescriptor {
                    offset:          mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format:          wgpu::VertexFormat::Float2,
                },
                wgpu::VertexAttributeDescriptor {
                    offset:          mem::size_of::<[f32; 5]>() as wgpu::BufferAddress,
                    shader_location: 2,
                    format:          wgpu::VertexFormat::Float2,
                },
                wgpu::VertexAttributeDescriptor {
                    offset:          mem::size_of::<[f32; 7]>() as wgpu::BufferAddress,
                    shader_location: 3,
                    format:          wgpu::VertexFormat::Float2,
                },
            ],
        }
    }
}
