#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec2 hidden_uv;
layout(location = 3) in vec2 shadow_uv;

layout(location = 0) out vec2 v_uv;
layout(location = 1) out vec2 v_shadow;

layout(set = 0, binding = 0) uniform Uniforms {
    mat4 view_proj;
    uint current_layer;
};

void main() {
    if (uint(position.y) == current_layer) {
        v_uv = hidden_uv;
    } else {
        v_uv = uv;
    }

    v_shadow = shadow_uv;

    gl_Position = view_proj * vec4(position, 1.0);
}