use cgmath::{ortho, Matrix4, Vector3};
use wgpu::SwapChainDescriptor;

#[rustfmt::skip]
pub const OPENGL_TO_WGPU_MATRIX: cgmath::Matrix4<f32> = cgmath::Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

#[derive(Debug)]
pub struct Camera {
    pub(crate) eye: cgmath::Point3<f32>,
    aspect:         f32,
    znear:          f32,
    zfar:           f32,
    width:          f32,
    height:         f32,
}

impl Camera {
    pub fn new(sc_desc: &SwapChainDescriptor) -> Camera {
        Camera {
            eye:    (256.0, 80.2, 256.0).into(),
            aspect: sc_desc.width as f32 / sc_desc.height as f32,
            znear:  0.1,
            zfar:   100.0,
            width:  sc_desc.width as f32,
            height: sc_desc.height as f32,
        }
    }

    pub fn build_view_projection_matrix(&self) -> cgmath::Matrix4<f32> {
        let view = Matrix4::look_at_dir(self.eye, -Vector3::unit_y(), -Vector3::unit_z());
        let proj = ortho(
            -8.0 * self.aspect,
            8.0 * self.aspect,
            -8.0,
            8.0,
            0.0,
            1000.0,
        );
        // let proj = perspective(Deg(90.0), self.aspect, self.znear, self.zfar);

        OPENGL_TO_WGPU_MATRIX * proj * view
    }
}
