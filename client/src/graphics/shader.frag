#version 450

layout(location = 0) in vec2 v_uv;
layout(location = 1) in vec2 v_shadow;

layout(location = 0) out vec4 f_color;

layout(set = 1, binding = 0) uniform texture2D tex;
layout(set = 1, binding = 1) uniform sampler sam;

void main() {
    vec4 tex_color = texture(sampler2D(tex, sam), v_uv);
    vec4 shadow_color = texture(sampler2D(tex, sam), v_shadow);
    float average = (shadow_color.r + shadow_color.g + shadow_color.b) / 3.0;
    vec4 shadow = vec4(average, average, average, 1.0);
    f_color = vec4(tex_color.rgb * shadow.rgb, tex_color.a);
}