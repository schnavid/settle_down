use crate::{graphics::Vertex, world::chunk::Chunk, GameContext};
use rayon::prelude::IntoParallelIterator;
use wgpu::{Buffer, BufferUsage};

#[rustfmt::skip]
fn autotiling_to_uv(autotiling: u8) -> [u16; 2] {
    #[allow(unreachable_patterns)]
    match autotiling {
        0 | 1 | 4 | 5 | 32 | 33 | 36 | 37 | 128 | 129 | 132 | 133 | 160 | 161 | 164 | 165 => [0, 0],
        16 | 17 | 20 | 21 | 48 | 49 | 52 | 53 | 144 | 145 | 148 | 149 | 176 | 177 | 180 | 181 => [8, 0],
        24 | 25 | 28 | 29 | 56 | 57 | 60 | 61 | 152 | 153 | 156 | 157 | 184 | 185 | 188 | 189 => [16, 0],
        8 | 9 | 12 | 13 | 40 | 41 | 44 | 45 | 136 | 137 | 140 | 141 | 168 | 169 | 172 | 173 => [24, 0],
        80 | 81 | 84 | 85 | 112 | 113 | 116 | 117 => [32, 0],
        72 | 73 | 76 | 77 | 200 | 201 | 204 | 205 => [40, 0],
        219 => [48, 0],

        64 | 65 | 68 | 69 | 96 | 97 | 100 | 101 | 192 | 193 | 196 | 197 | 224 | 225 | 228 | 229 => [0, 8],
        208 | 209 | 212 | 213 | 240 | 241 | 244 | 245 => [8, 8],
        248 | 249 | 252 | 253 => [16, 8],
        104 | 105 | 108 | 109 | 232 | 233 | 236 | 237 => [24, 8],
        18 | 19 | 50 | 51 | 146 | 147 | 178 | 179 => [32, 8],
        10 | 14 | 42 | 46 | 138 | 142 | 170 | 174 => [40, 8],
        126 => [48, 8],

        66 | 67 | 70 | 71 | 98 | 99 | 102 | 103 | 194 | 195 | 198 | 199 | 226 | 227 | 230 | 231 => [0, 16],
        214 | 215 | 246 | 247 => [8, 16],
        255 => [16, 16],
        107 | 111 | 235 | 239 => [24, 16],
        86 | 87 | 118 | 119 => [32, 16],
        75 | 79 | 203 | 207 => [40, 16],
        218 => [48, 16],

        2 | 3 | 6 | 7 | 34 | 35 | 38 | 39 | 130 | 131 | 134 | 135 | 162 | 163 | 166 | 167 => [0, 24],
        22 | 23 | 54 | 55 | 150 | 151 | 182 | 183 => [8, 24],
        31 | 63 | 159 | 191 => [16, 24],
        11 | 15 | 43 | 47 | 139 | 143 | 171 | 175 => [24, 24],
        82 | 83 | 114 | 115 => [32, 24],
        74 | 78 | 202 | 206 => [40, 24],
        94 => [48, 24],

        95 => [0, 32],
        120 | 121 | 124 | 125 => [8, 32],
        88 | 89 | 92 | 93 => [16, 32],
        216 | 217 | 220 | 221 => [24, 32],
        210 | 211 | 242 | 243 => [32, 32],
        106 | 110 | 234 | 238 => [40, 32],
        122 => [48, 32],

        250 => [0, 40],
        27 | 59 | 155 | 187 => [8, 40],
        26 | 58 | 154 | 186 => [16, 40],
        30 | 62 | 158 | 190 => [24, 40],
        127 => [32, 40],
        223 => [40, 40],
        91 => [48, 40],

        123 => [16, 48],
        222 => [24, 48],
        251 => [32, 48],
        254 => [40, 48],
        90 => [48, 48],

        _ => [8, 48],
    }
}

#[derive(Debug)]
pub struct ChunkRenderer {
    pub(crate) vertex_buffer: Option<Buffer>,
    pub(crate) num_vertices:  u32,
}

impl ChunkRenderer {
    pub fn new(chunk: &Chunk, ctx: &GameContext) -> ChunkRenderer {
        let (chunk_x, chunk_y, chunk_z) = (
            chunk.coordinates.x() as f32 * 16.0,
            chunk.coordinates.y() as f32 * 16.0,
            chunk.coordinates.z() as f32 * 16.0,
        );

        let vertices = chunk
            .iter_tiles()
            .into_par_iter()
            .enumerate()
            .map(|(idx, t)| {
                let y = idx / 16 / 16;
                let x = idx / 16 % 16;
                let z = idx % 16;
                (idx, x, y, z, t)
            })
            .map(|(_idx, x, y, z, t)| {
                let id = t.wall.id();
                if id.0 != 0 {
                    let uv = autotiling_to_uv(t.autotiling);

                    let (lt, rt, lb, rb) = (
                        [uv[0] as f32 / 256.0, uv[1] as f32 / 256.0],
                        [(uv[0] + 8) as f32 / 256.0, uv[1] as f32 / 256.0],
                        [uv[0] as f32 / 256.0, (uv[1] + 8) as f32 / 256.0],
                        [(uv[0] + 8) as f32 / 256.0, (uv[1] + 8) as f32 / 256.0],
                    );

                    let mut s_uv = autotiling_to_uv(t.shadow_autotiling);

                    s_uv[0] += 56;

                    let (s_lt, s_rt, s_lb, s_rb) = (
                        [s_uv[0] as f32 / 256.0, s_uv[1] as f32 / 256.0],
                        [(s_uv[0] + 8) as f32 / 256.0, s_uv[1] as f32 / 256.0],
                        [s_uv[0] as f32 / 256.0, (s_uv[1] + 8) as f32 / 256.0],
                        [(s_uv[0] + 8) as f32 / 256.0, (s_uv[1] + 8) as f32 / 256.0],
                    );

                    let h_uv = [uv[0], uv[1] + 56];

                    let (h_lt, h_rt, h_lb, h_rb) = (
                        [h_uv[0] as f32 / 256.0, h_uv[1] as f32 / 256.0],
                        [(h_uv[0] + 8) as f32 / 256.0, h_uv[1] as f32 / 256.0],
                        [h_uv[0] as f32 / 256.0, (h_uv[1] + 8) as f32 / 256.0],
                        [(h_uv[0] + 8) as f32 / 256.0, (h_uv[1] + 8) as f32 / 256.0],
                    );

                    vec![
                        Vertex {
                            position:  [
                                chunk_x + x as f32,
                                chunk_y + y as f32,
                                chunk_z + (z + 1) as f32,
                            ],
                            uv:        lb,
                            hidden_uv: h_lb,
                            shadow_uv: s_lb,
                        },
                        Vertex {
                            position:  [
                                chunk_x + (x + 1) as f32,
                                chunk_y + y as f32,
                                chunk_z + (z + 1) as f32,
                            ],
                            uv:        rb,
                            hidden_uv: h_rb,
                            shadow_uv: s_rb,
                        },
                        Vertex {
                            position:  [
                                chunk_x + (x + 1) as f32,
                                chunk_y + y as f32,
                                chunk_z + z as f32,
                            ],
                            uv:        rt,
                            hidden_uv: h_rt,
                            shadow_uv: s_rt,
                        },
                        Vertex {
                            position:  [
                                chunk_x + (x + 1) as f32,
                                chunk_y + y as f32,
                                chunk_z + z as f32,
                            ],
                            uv:        rt,
                            hidden_uv: h_rt,
                            shadow_uv: s_rt,
                        },
                        Vertex {
                            position:  [chunk_x + x as f32, chunk_y + y as f32, chunk_z + z as f32],
                            uv:        lt,
                            hidden_uv: h_lt,
                            shadow_uv: s_lt,
                        },
                        Vertex {
                            position:  [
                                chunk_x + x as f32,
                                chunk_y + y as f32,
                                chunk_z + (z + 1) as f32,
                            ],
                            uv:        lb,
                            hidden_uv: h_lb,
                            shadow_uv: s_lb,
                        },
                    ]
                } else {
                    vec![]
                }
            })
            .flatten()
            .collect::<Vec<_>>();

        if !vertices.is_empty() {
            let vertex_buffer = ctx.gfx.device.create_buffer_with_data(
                bytemuck::cast_slice(vertices.as_slice()),
                BufferUsage::VERTEX,
            );

            ChunkRenderer {
                vertex_buffer: Some(vertex_buffer),
                num_vertices:  vertices.len() as u32,
            }
        } else {
            ChunkRenderer {
                vertex_buffer: None,
                num_vertices:  0,
            }
        }
    }
}
