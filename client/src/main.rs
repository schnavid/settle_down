use futures::SinkExt;
use settle_down_core::network::{codec::SettleDownCodec, packet::Packet};
use tokio::{net::TcpStream, stream::StreamExt};
use tokio_util::codec::Framed;

type Result<T> = settle_down_core::Result<T>;

#[tokio::main]
async fn main() -> crate::Result<()> {
    let stream = TcpStream::connect("127.0.0.1:8080").await?;

    let codec = SettleDownCodec::new();
    let mut framed = Framed::new(stream, codec);

    framed.send(Packet::Login("schnavid".to_string())).await?;
    loop {
        println!("{:?}", framed.next().await);
    }
}
